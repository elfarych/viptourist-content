import axios from 'axios'
import server from 'src/config/server'

export const notificationLinks = {
  getNotificationLinks: async function () {
    if (!this.links.length) {
      this.links = await axios.get(`${server.notificationServerURI}/links`)
        .then(res => res.data || [])
    }
    return this.links
  },

  getNotificationCycles: async function () {
    if (!this.cycles.length) {
      this.cycles = await axios.get(`${server.notificationServerURI}/cycles`)
        .then(res => res.data || [])
    }
    return this.cycles
  },
  cycles: [],
  links: []
}
