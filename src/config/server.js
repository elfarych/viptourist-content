export default {
  serverURI: 'https://server.viptourist.club',
  // serverURI: 'http://192.168.0.109:1337',
  notificationServerURI: 'https://notification.viptourist.club',
  // notificationServerURI: 'http://192.168.0.109:1338',
  headers: {
    Authorization: ''
  },

  setHeaders (jwt) {
    this.headers.Authorization = `Bearer ${jwt}`
  }
}
