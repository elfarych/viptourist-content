import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import user from './modules/user'
import tours from './modules/tours'
import offers from './modules/offers'
import tourists from './modules/tourists'
import guides from './modules/guides'
import notifications from './modules/notifications'
import generalNotifications from './modules/general-notifications'
import messages from './modules/messages'
import places from './modules/places'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      user,
      tours,
      offers,
      tourists,
      guides,
      notifications,
      generalNotifications,
      messages,
      places
    },

    strict: process.env.DEBUGGING
  })

  return Store
}
