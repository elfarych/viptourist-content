import errorHandler from 'src/utils/errorHandler'
import axios from 'axios'
import server from 'src/config/server'
import notifier from 'src/utils/t-notifier'

export async function loadMessages ({ commit }) {
  try {
    await axios.get(`${server.notificationServerURI}/messages`)
      .then(res => {
        return commit('mutationMessages', res.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function createMessage ({ dispatch }, payload) {
  try {
    await axios.post(`${server.notificationServerURI}/messages`, {
      ...payload,
      locale: 'ru-RU'
    })
      .then(res => {
        notifier({ message: 'Массовое сообщение успешно создано.', color: 'positive' })
        return dispatch('loadMessages')
      })
  } catch (err) {
    errorHandler(err)
  }
}
