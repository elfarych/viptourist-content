import errorHandler from 'src/utils/errorHandler'
import axios from 'axios'
import server from 'src/config/server'
import notifier from 'src/utils/t-notifier'

export async function loadGeneralNotifications ({ commit }) {
  try {
    await axios.get(`${server.notificationServerURI}/general-notifications`)
      .then(res => {
        return commit('mutationGeneralNotifications', res.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function deleteNotification ({ dispatch }, id) {
  const confirmation = confirm('Удалить уведомление?')
  if (!confirmation) return
  try {
    await axios.delete(`${server.notificationServerURI}/general-notifications/${id}`)
      .then(res => {
        notifier({ message: 'Уведомление успешно удалено.', color: 'positive' })
        return dispatch('loadGeneralNotifications')
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function createGeneralNotification ({ dispatch }, payload) {
  try {
    await axios.post(`${server.notificationServerURI}/general-notifications`, {
      ...payload
    })
      .then(res => {
        notifier({ message: 'Уведомление успешно создано.', color: 'positive' })
        return dispatch('loadGeneralNotifications')
      })
  } catch (err) {
    errorHandler(err)
  }
}
