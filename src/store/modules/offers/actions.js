import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

export async function loadOffers ({ commit }, params) {
  try {
    await axios
      .get(`${server.serverURI}/offers`, {
        headers: server.headers,
        params: { ...params }
      })
      .then(response => {
        commit('mutationOffers', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadOffer ({ commit }, id) {
  if (!id) id = this.$router.currentRoute.params.id
  try {
    await axios
      .get(`${server.serverURI}/offers/${id}`,
        {
          headers: server.headers
        })
      .then(response => {
        return commit('mutationOffer', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
