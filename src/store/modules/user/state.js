export default function () {
  return {
    user: null
  }
}

// USER
// "user": {
//   "confirmed": true,
//     "blocked": false,
//     "_id": "615949c227893f004d8f5cf5",
//     "username": "Eldar",
//     "email": "eldar@mail.ru",
//     "provider": "local",
//     "createdAt": "2021-10-03T06:12:18.308Z",
//     "updatedAt": "2021-10-03T06:32:50.045Z",
//     "__v": 0,
//     "role": {
//     "_id": "61594b3527893f004d8f5cf6",
//       "name": "Translator",
//       "description": "Content translator",
//       "type": "translator",
//       "__v": 0,
//       "id": "61594b3527893f004d8f5cf6"
//   },
//   "i_18_n_locale": "61583cefc98756024aa945cf",
//     "languages": [
//     {
//       "_id": "61583cefc98756024aa945cf",
//       "code": "en",
//       "name": "English (en)",
//       "createdAt": "2021-10-02T11:05:19.271Z",
//       "updatedAt": "2021-10-03T06:32:50.043Z",
//       "__v": 0,
//       "translator": "615949c227893f004d8f5cf5",
//       "id": "61583cefc98756024aa945cf"
//     },
//     {
//       "_id": "61583e04c98756024aa947aa",
//       "name": "Russian (Russia) (ru-RU)",
//       "code": "ru-RU",
//       "createdAt": "2021-10-02T11:09:56.384Z",
//       "updatedAt": "2021-10-03T06:32:50.043Z",
//       "__v": 0,
//       "translator": "615949c227893f004d8f5cf5",
//       "id": "61583e04c98756024aa947aa"
//     }
//   ],
//     "id": "615949c227893f004d8f5cf5"
// }
