import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
// import notifier from 'src/utils/notifier'

export async function loadTourists ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/profiles`, {
        params: {
          is_tourist: true
        }
      })
      .then(response => {
        commit('mutationTourists', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadTourist ({ commit }, uid) {
  try {
    await axios
      .get(`${server.serverURI}/profiles/${uid}`, {
        headers: server.headers
      })
      .then(response => {
        return commit('mutationTourist', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
