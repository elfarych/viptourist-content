import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import notifier from 'src/utils/notifier'

export async function loadTours ({ commit }, params) {
  try {
    await axios
      .get(`${server.serverURI}/tours`, {
        headers: server.headers,
        params: { ...params }
      })
      .then(response => {
        commit('mutationTours', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadUnapprovedTours ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/tours`, {
        headers: server.headers,
        params: {
          _next: false, // Оригинальный тур
          _locale: 'all',
          _approved: false
        }
      })
      .then(response => {
        commit('mutationUnapprovedTours', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function updateTour ({ commit }, data) {
  try {
    await axios
      .put(`${server.serverURI}/tours/${data.id}`, {
        ...data
      }, {
        headers: server.headers
      })
      .then(response => {
        commit('mutationTour', response.data)
        return notifier({ message: 'Тур обновлен.', color: 'positive' })
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadTour ({ commit }, id) {
  if (!id) id = this.$router.currentRoute.params.id
  try {
    await axios
      .get(`${server.serverURI}/tours/${id}`,
        {
          headers: server.headers
        })
      .then(response => {
        return commit('mutationTour', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
