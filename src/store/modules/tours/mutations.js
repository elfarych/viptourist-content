export function mutationTours (state, data) {
  state.tours = data.map(i => {
    return {
      ...i,
      cityName: i.city?.name || '',
      guideName: i.profile?.name || ''
    }
  })
}

export function mutationUnapprovedTours (state, data) {
  state.unapprovedTours = data
}

export function mutationTour (state, data) {
  state.tour = data
}
