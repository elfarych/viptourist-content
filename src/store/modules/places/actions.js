import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
// import notifier from 'src/utils/notifier'

export async function loadCountries ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/countries`, {
        headers: server.headers
      })
      .then(response => {
        commit('mutationCountries', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadCities ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/cities`, {
        headers: server.headers
      })
      .then(response => {
        commit('mutationCities', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
