import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
import notifier from 'src/utils/t-notifier'
// import notifier from 'src/utils/notifier'

export async function loadNotifications ({ commit }) {
  const uid = this.$router.currentRoute.params.id
  try {
    await axios
      .get(`${server.notificationServerURI}/notifications`, {
        params: {
          '_profile.uid': uid
        }
      })
      .then(response => {
        commit('mutationNotifications', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function deleteNotification ({ commit, dispatch }, id) {
  try {
    await axios.delete(`${server.notificationServerURI}/notifications/${id}`)
      .then(async response => {
        notifier({ message: 'Уведомление успешно удалено.', color: 'positive' })
        dispatch('loadNotifications')
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function createNotification ({ commit, dispatch }, payload) {
  const profile = await axios.get(`${server.notificationServerURI}/profiles`, {
    params: {
      _uid: payload.uid
    }
  }).then(res => res.data[0] || null)
  if (!profile) {
    return notifier({ message: 'Не удалось создать уведомление.', color: 'negative' })
  }
  try {
    await axios
      .post(`${server.notificationServerURI}/notifications`, {
        title: payload.title,
        body: payload.body,
        link: payload.link,
        image: payload.image,
        profile: profile.id
      })
      .then(async response => {
        notifier({ message: 'Уведомление успешно создано.', color: 'positive' })
        dispatch('loadNotifications')
      })
  } catch (err) {
    errorHandler(err)
  }
}
