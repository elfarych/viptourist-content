export function mutationGuides (state, data) {
  state.guides = data
}

export function mutationUnverifiedGuides (state, data) {
  state.unverifiedGuides = data
}

export function mutationGuid (state, data) {
  state.guid = data
}
