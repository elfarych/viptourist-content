import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'
// import notifier from 'src/utils/notifier'

export async function loadGuides ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/profiles`, {
        params: {
          is_tourist: false
        }
      })
      .then(response => {
        commit('mutationGuides', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadUnverifiedGuides ({ commit }) {
  try {
    await axios
      .get(`${server.serverURI}/profiles`, {
        params: {
          is_tourist: false,
          is_verified: false
        }
      })
      .then(response => {
        commit('mutationUnverifiedGuides', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function loadGuid ({ commit }, uid) {
  try {
    await axios
      .get(`${server.serverURI}/profiles/${uid}`, {
        headers: server.headers
      })
      .then(response => {
        return commit('mutationGuid', response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
