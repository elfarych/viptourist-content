// This is just an example,
// so you can safely delete all default props below

export default {
  Quit: 'Quit',
  Home: 'Home',
  Tours: 'Tours',
  Offers: 'Offers',
  Countries: 'Countries',
  Cities: 'Cities',
  SuccessAuthorization: 'Authorization success.',
  SuccessLogged: 'Login success.',
  TableTourTitle: 'Tour',
  TableLocalesTitle: 'Translate',
  hasTranslate: 'Translated',
  noTranslate: 'No translate',
  original: 'Original',
  TableOriginalLocaleTitle: '',
  onlyNoTranslations: 'No translation',
  search: 'Search',
  backRoute: 'Back',
  save: 'Save',
  cancel: 'Cancel',
  tourUpdated: 'Tour updated.',
  tourTitle: 'Tour title',
  tourDescription: 'Tour description',
  tourCreated: 'Tour translate created.'
}
