import Vue from 'vue'
import dayjs from 'dayjs'

require('dayjs/locale/ru')
dayjs.locale('ru')

const updateLocale = require('dayjs/plugin/updateLocale')
const quarterOfYear = require('dayjs/plugin/quarterOfYear')
// импортируем плагины
import customParseFormat from 'dayjs/plugin/customParseFormat'
import utc from 'dayjs/plugin/utc'

// подключаем плагины
dayjs.extend(customParseFormat)
dayjs.extend(utc)
dayjs.extend(updateLocale)
dayjs.extend(quarterOfYear)

dayjs.updateLocale('ru', {
  monthsShort: [
    'янв', 'фев', 'мар', 'апр', 'май', 'июн',
    'июл', 'авг', 'сен', 'окт', 'ноя', 'дек'
  ]
})

Vue.prototype.$dayjs = dayjs

export { dayjs }
