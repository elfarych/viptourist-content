import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

export async function loadModel ({ modelName, callback, id }) {
  try {
    await axios
      .get(`${server.serverURI}/${modelName}/${id}`, {
        headers: server.headers
      })
      .then(response => {
        return callback(response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}

export async function updateOriginalModel ({ modelName, callback, data, vm }) {
  const { id } = vm.$router.currentRoute.params
  try {
    await axios
      .put(`${server.serverURI}/${modelName}/${id}`, {
        ...data
      }, {
        headers: server.headers
      })
      .then(response => {
        return callback(response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
