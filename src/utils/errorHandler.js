import notifier from 'src/utils/notifier'

export default function errorHandler (err) {
  notifier({ message: err?.message || err })
  return err
}
