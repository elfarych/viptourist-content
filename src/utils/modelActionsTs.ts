import axios from 'axios'
import errorHandler from 'src/utils/errorHandler'
import server from 'src/config/server'

interface Model {
  modelName: string;
  callback: any;
  id: string
}

export async function loadModel (model: Model) {
  try {
    await axios
      .get(`${server.serverURI}/${model.modelName}/${model.id}`, {
        headers: server.headers
      })
      .then(response => {
        return model.callback(response.data)
      })
  } catch (err) {
    errorHandler(err)
  }
}
