export default function getCycleName (name) {
  switch (name) {
    case 'profileVerified':
      return 'Профиль верфицирован (для гида)'

    case 'remarkCreated':
      return 'Замечание к офферу (для гида)'

    case 'offerApproved':
      return 'Тур проверен (для гида)'

    case 'orderCreated':
      return 'Новый заказ (для гида)'

    case 'orderSellerApproved':
      return 'Продавец подтвердил заказ (для туриста)'

    case 'orderCancelled':
      return 'Заказ отменен (для гида и туриста)'
  }
}
