import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState('guides', ['unverifiedGuides']),
    ...mapState('tours', ['unapprovedTours']),
    notApprovedTours () {
      return this.unapprovedTours.filter(i => !i.approved)
    },
    notApprovedGuides () {
      return this.unverifiedGuides.filter(i => !i.is_verified)
    },
    links () {
      const vm = this
      return [
        { routeName: 'countries', title: 'Страны', icon: 'las la-globe' },
        { routeName: 'cities', title: 'Города', icon: 'las la-city' },
        { routeName: 'tours', title: 'Туры', icon: 'las la-map-marked', count: vm.notApprovedTours.length },
        { routeName: 'guides', title: 'Гиды', icon: 'las la-user', count: vm.notApprovedGuides.length },
        { routeName: 'tourists', title: 'Туристы', icon: 'las la-user' },
        { routeName: 'notifications', title: 'Уведомления', icon: 'las la-comment' },
        { routeName: 'messages', title: 'Массовые сообщения', icon: 'las la-comment' }
      ]
    }
  }
}
