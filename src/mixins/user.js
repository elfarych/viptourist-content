import { mapState } from 'vuex'

export default {
  computed: {
    ...mapState('user', ['user']),
    language () {
      return localStorage.getItem('language')
    }
  },

  watch: {

  }
}
