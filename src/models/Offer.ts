interface OfferInterface {
  languages?: any;
  prerequisites?: any;
  prohibitions?: any;
  included?: any;
  // eslint-disable-next-line camelcase
  not_included?: any;
  note?: any;
}

export default OfferInterface
