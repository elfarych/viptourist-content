export interface LocalizationInterface {
  id: string;
  locale: string
}

export interface TourInterface {
  id: string;
  name: string;
  description: string;
  translationApproved: boolean;
  locale: string;
  createdLanguage: string;
  localizations: [LocalizationInterface]
}
