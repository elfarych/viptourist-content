
export default class Offer {
  constructor (offer) {
    this.adult_price = offer.adult_price
    this.alwaysAvailable = offer.alwaysAvailable
    this.approved = offer.approved
    this.child_price = offer.child_price
    this.createdAt = offer.createdAt
    this.createdLanguage = offer.createdLanguage
    this.date = offer.date
    this.duration = offer.duration
    this.id = offer.id
    this.image = offer.image
    this.image_urls = offer.image_urls
    this.included = offer.included
    this.languages = offer.languages
    this.locale = offer.locale
    this.localizations = offer.localizations || []
    this.not_included = offer.not_included
    this.note = offer.note
    this.prerequisites = offer.prerequisites
    this.profile = offer.profile || {}
    this.prohibitions = offer.prohibitions
    this.top = offer.top
    this.tour = offer.tour || {}
    this.translationApproved = offer.translationApproved
    this.updatedAt = offer.updatedAt
    this.vid = offer.vid
    this.remark = offer.remark
  }

  getImages () {
    return this.image_urls?.split(',') || []
  }

  deleteImage (image) {
    const imagesArr = this.getImages()
    if (!confirm('Удалить фото?')) return
    imagesArr.splice(imagesArr.indexOf(image), 1)
    this.image_urls = imagesArr.join(',')
  }
}
