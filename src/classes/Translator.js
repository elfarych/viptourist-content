import { Translate } from '@google-cloud/translate/build/src/v2'
import errorHandler from 'src/utils/errorHandler'

export class Translator {
  key = 'AIzaSyByQhNY4yuZa4GiMCSIZP8uIznijKFYf5Q';
  translate = new Translate({ key: this.key });

  async translateText ({ text = [], target = '' }) {
    let translations = []
    try {
      [translations] = await this.translate.translate(text, target)
    } catch (err) {
      errorHandler(err)
    }

    return translations
  }
}
