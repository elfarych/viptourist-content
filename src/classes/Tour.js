
export default class Tour {
  constructor (tour) {
    this.id = tour.id
    this._id = tour._id
    this.name = tour.name
    this.image = tour.image
    this.rating = tour.rating
    this.order = tour.order
    this.top = tour.top
    this.guide = tour.guide
    this.private = tour.private
    this.one_day_trip = tour.one_day_trip
    this.nature = tour.nature
    this.ticket_must_have = tour.ticket_must_have
    this.on_water = tour.on_water
    this.package_tour = tour.package_tour
    this.small_group = tour.small_group
    this.invalid_friendly = tour.invalid_friendly
    this.history = tour.history
    this.world_war = tour.world_war
    this.open_air = tour.open_air
    this.street_art = tour.street_art
    this.adrenaline = tour.adrenaline
    this.architecture = tour.architecture
    this.food = tour.food
    this.music = tour.music
    this.for_couples_activities = tour.for_couples_activities
    this.for_kids_activities = tour.for_kids_activities
    this.museum = tour.museum
    this.memorial = tour.memorial
    this.park = tour.park
    this.gallery = tour.gallery
    this.square = tour.square
    this.theater = tour.theater
    this.castle = tour.castle
    this.towers = tour.towers
    this.airports = tour.airports
    this.bicycle = tour.bicycle
    this.minivan = tour.minivan
    this.public_transport = tour.public_transport
    this.limousine = tour.limousine
    this.bicycle_taxi = tour.bicycle_taxi
    this.car = tour.car
    this.cruise = tour.cruise
    this.offers = tour.offers
    this.city = tour.city
    this.vid = tour.vid
    this.reviews_count = tour.reviews_count
    this.price = tour.price
    this.child_price = tour.child_price
    this.reviews = tour.reviews
    this.country = tour.country
    this.image_urls = tour.image_urls
    this.approved = tour.approved
    this.translationApproved = tour.translationApproved
    this.createdLanguage = tour.createdLanguage
    this.description = tour.description
    this.location_point = tour.location_point
    this.remark = tour.remark
    this.localizations = tour.localizations
    this.locale = tour.locale
    this.profile = tour.profile
    this.duration = tour.duration
    this.languages = tour.languages
    this.prerequisites = tour.prerequisites
    this.prohibitions = tour.prohibitions
    this.included = tour.included
    this.not_included = tour.not_included
    this.alwaysAvailable = tour.alwaysAvailable
    this.withTransfer = tour.withTransfer
    this.note = tour.note
    this.freeTicketNotice = tour.freeTicketNotice
    this.weekDays = tour.weekDays
    this.mainPhotoUrl = tour.mainPhotoUrl
    this.transferPhotoUrl = tour.transferPhotoUrl
  }

  getImages () {
    return this.image_urls?.split('|') || []
  }

  getWeekDays () {
    const days = this.weekDays
      .replace('mn', 'понедельник')
      .replace('tu', 'вторник')
      .replace('wd', 'среда')
      .replace('th', 'четверг')
      .replace('fr', 'пятница')
      .replace('st', 'суббота')
      .replace('sn', 'воскресенье')

    return days.split(',').join(' ')
  }

  deleteImage (image) {
    const imagesArr = this.getImages()
    if (!confirm('Удалить фото?')) return
    imagesArr.splice(imagesArr.indexOf(image), 1)
    this.image_urls = imagesArr.join('|')
  }
}
