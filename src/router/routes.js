
const routes = [
  {
    path: '/',
    redirect: {
      name: 'home'
    }
  },
  {
    path: '/home',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'home', component: () => import('pages/Index.vue') }
    ]
  },

  {
    path: '/tours',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'tours', component: () => import('pages/Tours/Tours') },
      { path: ':id', name: 'tour-detail', component: () => import('pages/Tours/TourDetail') }
    ]
  },

  {
    path: '/offers',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'offers', component: () => import('pages/Offers/Offers') },
      { path: ':id', name: 'offer-detail', component: () => import('pages/Offers/OfferDetail') }
    ]
  },

  {
    path: '/countries',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'countries', component: () => import('pages/Countries') }
    ]
  },

  {
    path: '/cities',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'cities', component: () => import('pages/Cities') }
    ]
  },

  {
    path: '/guides',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'guides', component: () => import('pages/Guides/Guides') },
      { path: ':id', name: 'guid', component: () => import('pages/Guides/Guid') }
    ]
  },

  {
    path: '/messages',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'messages', component: () => import('pages/Messages/Messages') }
    ]
  },

  {
    path: '/tourists',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'tourists', component: () => import('pages/Tourists/Tourists') },
      { path: ':id', name: 'tourist', component: () => import('pages/Tourists/Tourist') }
    ]
  },

  {
    path: '/login',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'login', component: () => import('pages/Login') }
    ]
  },

  {
    path: '/notifications',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'notifications', component: () => import('pages/Notifications/Notifications') },
      { path: ':id', name: 'notification-detail', component: () => import('pages/Notifications/NotificationDetail') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
